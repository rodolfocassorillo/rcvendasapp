import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProdutosService } from '../../services/produtos/produtos.service';
import { Produto } from '../../interfaces/produto';
import { DatabaseService } from 'src/app/services/database/database.service';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.page.html',
  styleUrls: ['./produtos.page.scss'],
})
export class ProdutosPage implements OnInit {

  produtos: Produto[] = [];

  produto = {};

  selectedView = 'produtos';

  constructor(private produtosService: ProdutosService,
    private db: DatabaseService) { }

  ngOnInit() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getProdutos().subscribe(prods => {
          this.produtos = prods;
        })
      }
    });
  }

}
