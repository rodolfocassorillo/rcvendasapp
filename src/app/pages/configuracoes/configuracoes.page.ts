import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToastController } from '@ionic/angular';
import { themes } from '../../interfaces/themes';

@Component({
  selector: 'app-configuracoes',
  templateUrl: './configuracoes.page.html',
  styleUrls: ['./configuracoes.page.scss'],
})
export class ConfiguracoesPage implements OnInit {

  public nigthMode: boolean;
  public modelo: string;
  public geoData: string = '';

  constructor(public theme: ThemeService,
              public geolocation: Geolocation,
              private toastCtrl: ToastController) {    
    this.theme.storedTheme.then(cssText => {
      if (cssText['night'] === themes['night']){
        this.nigthMode = true;
        this.modelo = 'night';
      }else{
        this.nigthMode = false;
        this.modelo = 'autumn';
      }
    });
   }

  changeTheme() {
    this.modelo = this.nigthMode ? 'night' : 'autumn';
    this.theme.setTheme(themes[this.modelo]);
  }

  locate(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      this.geoData = 'Latitude: '+ resp.coords.latitude + ' Longitude: '+ resp.coords.longitude; 
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     console.log(this.geoData);
     this.presentToast('Sua localização atual é: '+this.geoData);
  }

  ngOnInit() {
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

}
