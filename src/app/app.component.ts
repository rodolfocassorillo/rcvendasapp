import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { ThemeService } from './services/theme/theme.service';
import { themes } from './interfaces/themes';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/pages/home',
      icon: 'podium'
    },
    {
      title: 'List',
      url: '/pages/list',
      icon: 'list'
    },    
    {
      title: 'Clientes',
      url: '/pages/clientes',
      icon: 'settings'
    },    
    {
      title: 'Produtos',
      url: '/pages/produtos',
      icon: 'settings'
    },    
    {
      title: 'Portifolio',
      url: '/pages/portifolio',
      icon: 'settings'
    },    
    {
      title: 'Transmissão',
      url: '/pages/transmissao',
      icon: 'settings'
    },    
    {
      title: 'Vendas',
      url: '/pages/vendas',
      icon: 'cart'
    },    
    {
      title: 'Configurações',
      url: '/pages/configuracoes',
      icon: 'settings'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private push: Push,
    public theme: ThemeService
  ) {
    this.initializeApp();
    this.theme.storedTheme.then(cssText => {
      if (!cssText) {
        this.theme.setTheme(themes['night']);
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.pushSetup();
    });
  }

  goPage(pageName: string){
    this.router.navigate([pageName]);
    // this.router.navigateByUrl(pageName);
  }

  pushSetup(){
    const options: PushOptions = {
      android: {
        senderID: '707126383959',
        vibrate: 'true',
        sound: 'true'
      },
      ios: {
          alert: 'true',
          badge: 'true',
          sound: 'false'
      }
   }
   
   const pushObject: PushObject = this.push.init(options);
   
   
   pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
   
   pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
   
   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

}
