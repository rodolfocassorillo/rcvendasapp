import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuard]
  },
  {
    path: 'list',
    loadChildren: './pages/list/list.module#ListPageModule',  canActivate: [AuthGuard]
  },
  { path: 'produtos', loadChildren: './pages/produtos/produtos.module#ProdutosPageModule',  canActivate: [AuthGuard] },
  { path: 'clientes', loadChildren: './pages/clientes/clientes.module#ClientesPageModule',  canActivate: [AuthGuard] },
  { path: 'configuracoes', loadChildren: './pages/configuracoes/configuracoes.module#ConfiguracoesPageModule',  canActivate: [AuthGuard] },
  { path: 'transmissao', loadChildren: './pages/transmissao/transmissao.module#TransmissaoPageModule',  canActivate: [AuthGuard] },
  { path: 'vendas', loadChildren: './pages/vendas/vendas.module#VendasPageModule',  canActivate: [AuthGuard] },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule', canActivate: [LoginGuard] },
  { path: 'portifolio', loadChildren: './pages/portifolio/portifolio.module#PortifolioPageModule',  canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
