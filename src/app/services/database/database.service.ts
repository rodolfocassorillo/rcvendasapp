import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { Produto } from '../../interfaces/produto';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public importJson: any;

  produtos = new BehaviorSubject([]);

  constructor(private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'rcvendasapp.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('start import json: ');
          this.createDatabase();
          this.seedDatabase();
          console.log('preencheu com o json: ');
        });
    });
  }

  createDatabase() {
    this.http.get('../../../assets/database/seed.sql', { responseType: 'text' })
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(_ => {
            this.dbReady.next(true);
          })
          .catch(e => console.error(e));
      });
  }

  //implementar ionic file to download the json into asset/database and then use seed to import the JSON's files
  seedDatabase() {
    this.http.get('http://192.168.25.10:8043/ords/kdb1/produtos/tabela/2', { responseType: 'text' })
      .subscribe(json => {
        this.sqlitePorter.importJsonToDb(this.database, json)
          .then(_ => {
            this.dbReady.next(true);
          })
          .catch(e => console.error(e));
      });
    this.loadProdutos();
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  getProdutos(): Observable<Produto[]> {
    return this.produtos.asObservable();
  }

  loadProdutos() {
    return this.database.executeSql(' SELECT p.* FROM items p ', []).then(data => {
      let prods: Produto[] = [];

      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let skills = [];
          if (data.rows.item(i).skills != '') {
            skills = JSON.parse(data.rows.item(i).skills);
          }

          prods.push({
            cd_produto: data.rows.item(i).cd_produto,
            ds_produto: data.rows.item(i).ds_produto,
            ds_marca: data.rows.item(i).ds_marca,
            qt_multiplo: data.rows.item(i).qt_multiplo,
            cd_tabelapreco: data.rows.item(i).cd_tabelapreco,
            vl_bruto_produto: data.rows.item(i).vl_bruto_produto,
            pc_desconto: data.rows.item(i).pc_desconto,
            cd_unidade_venda: data.rows.item(i).cd_unidade_venda,
            qt_produtoembalagem: data.rows.item(i).qt_produtoembalagem,
            qt_estoque_atual: data.rows.item(i).qt_estoque_atual,
            tp_item: data.rows.item(i).tp_item
          });
        }
      }
      this.produtos.next(prods);
    });
  }


}
