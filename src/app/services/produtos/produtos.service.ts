import { Injectable } from '@angular/core';
import { Produto } from '../../interfaces/produto';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { parBaseURL } from '../../interfaces/parametros';
import { parKeyJSON } from '../../interfaces/parametros';
import { parCdTabelaPrecoPadrao } from '../../interfaces/parametros';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {

  constructor(private httpClient: HttpClient) { }

  // Sending a GET request to /products

  public getProdutos() {
    return this.httpClient.get(parBaseURL + '/produtos/tabela/' + parCdTabelaPrecoPadrao).pipe(
      map(results => results[parKeyJSON])
    );
  }

  public getJSon() :any{
    return this.httpClient.get('../../../assets/database/db.json').pipe(map(res => res[parKeyJSON]));
  }

  // Sending a POST request to /products

  public createProduct(produto: Produto) {

  }

  // Sending a GET request to /products/:id

  public getProductById(produtoId: number) {

  }

  // Sending a PUT request to /products/:id

  public updateProduct(produto: Produto) {

  }

  // Sending a DELETE request to /products/:id

  public deleteProductById(produtoId: number) {

  }

}
