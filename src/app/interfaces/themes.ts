export const themes = {
    night: {
      primary: '#1b1b1b',
      secondary: '#29b6f6',
      tertiary: '#FE5F55',
      medium: '#6d6d6d',
      dark: '#FFFFFF',
      light: '#424242'
    },
    autumn: {
        // primary: '#F78154',
        // secondary: '#4D9078',
        // tertiary: '#B4436C',
        // light: '#FDE8DF',
        // medium: '#FCD0A2',
        // dark: '#B89876'
      },
      night2: {
        primary: '#BCC2C7',
        secondary: '#FCFF6C',
        tertiary: '#FE5F55',
        medium: '#BCC2C7',
        dark: '#ffffff',
        light: '#363232'
      }  
  };