export class Produto {

    cd_produto?: number;
    ds_produto?: string;
    ds_marca?: string;
    qt_multiplo?: number;
    cd_tabelapreco?: number;
    vl_bruto_produto?: number;
    pc_desconto?: number;
    cd_unidade_venda?: string;
    qt_produtoembalagem?: number;
    qt_estoque_atual?: number;
    tp_item?: string;
    
    // constructor(values: Object = {}) {
    
    // Object.assign(this, values);
    
    // }
    
}